import psycopg2
from database.config.config import config
import database.insert_data.parse_article_from_file as parse
import time

# http://www.postgresqltutorial.com/postgresql-python/query/
def select_data(sql):
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()

        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)

        # create a cursor
        cur = conn.cursor()

        # execute a statement
        cur.execute(sql)

        row = cur.fetchone()
        while row is not None:
            row = cur.fetchone()
        # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print("ERROR:"+error)
    finally:
        if conn is not None:
           return row
           conn.close()

def insert_single_article(article):
    # time 2019-01-01 10:00:00 +3:00
    # seq: err_news_orig_id_seq'
    """ Insert article to table """
    ''' sql = """
    INSERT INTO err_news_orig(id, err_id, author, url, lead, content, category, date_published, editor)
             VALUES(
             nextval('err_news_orig_id_seq'),
             123,
             'testauthor', 
             'testurl', 
             'testlead', 
             'testcontent', 
             'testcategory', 
             '2019-01-01 10:00:00 +3:00', 
             'testeditor')
             ;
             """
    '''

    # check data is not null:
    for key, val in article.items():
        if type(val) is str and (val is "" or "NULL" in val or "nullValue" in val):
            article[key] = "NULL"
        elif type(val) is int:
            article[key] = int(val)
        else:
            article[key] = "'"+article[key]+"'"

    sql = "INSERT INTO err_news_orig(id, err_id, author, url, lead, content, category, date_published, editor, headline, language) " \
          "VALUES(nextval('err_news_orig_id_seq'), {}, {}, {}, {}, {}, {}, {}, {}, {}, {});"\
        .format(
              article["errID"],
              article["author"],
              article["url"],
              article["lead"],
              article["content"],
              article["category"],
              article["datePublished"],
              article["editor"],
              article["headline"],
              article["language"]
        )
    # print(article)
    # print(sql)
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        print(article["errID"])
        exit(0)
    finally:
        if conn is not None:
            conn.close()

start_time = time.time()
# exec: insert all articles to db
dir = parse.dir
files_in_directory = parse.files_as_list(dir) # as list
print(files_in_directory)
count = len(files_in_directory)
for i in range(count):
    article = parse.get_article_from_file(dir, files_in_directory[i])
    insert_single_article(article)
    if i % 1000 == 0:
        print(str(i)+"|"+str(time.time()-start_time))
print("Total exec time:"+str(time.time()-start_time))

