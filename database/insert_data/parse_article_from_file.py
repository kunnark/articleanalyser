import os
from io import StringIO
from lxml import etree
from lxml import html
import database.model.article
import os


dir = "/Users/kunnarkukk/PycharmProjects/ScrapyThesis/err/err/database/2019-06-28"

def files_as_list(dir):
    list_of_files = []
    if os.path.exists(dir):
        list_of_files = os.listdir(dir)
    if list_of_files is not None:
        return list_of_files
    else:
        return None

def get_article_from_file(dir, filename):
    if "ee" or "www" in filename:
        pass
    f = open(dir+"/"+filename, "r", encoding="utf-8", errors='ignore')

    article = database.model.article.Article
    article = article.get_as_dict(article)

    if f is not None:
        content = (f.read()).strip()
        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(content), parser)

        if tree is not None:
            # Make correct string to parse
            try:
                htmlstring = etree.tostring(tree.getroot(), pretty_print=True, method="html")

                pre = "/html/body/"
                text = "/text()"

                # map article with file content
                root = html.fromstring(htmlstring)
                for key in article:
                    # error: print(item)
                    item = root.xpath(pre + str(key).lower() + text)
                    if len(item) > 0 and item[0].strip() is not "nullValue":
                        if key is "errID":
                            article[key] = int(item[0])
                        else:
                            article[key] = item[0].strip().replace("nullValue", "NULL")
                            article[key] = str(item[0]).strip().replace("'", "#")
                    elif (len(item) > 0 and "nullValue" in item[0].strip()) or len(item) == 0:
                        article[key] = "NULL"
                    else:
                        article[key] = "NULL"
            except(Exception):
                pass
            f.close()

    # Placeholder for last file written:
    fo = open(dir+"/"+"lastindex.txt", "w+", encoding="utf-8", errors='ignore')
    fo.write(filename)
    fo.close()
    return article


