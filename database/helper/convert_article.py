import re

def single_article_as_string(article_as_comma_separated_list, delimitter) -> str:
    article_as_comma_separated_list = article_as_comma_separated_list.replace(",", delimitter)
    article_as_comma_separated_list = article_as_comma_separated_list.lower()
    response = re.findall('[a-zõäöü]+[ ]', article_as_comma_separated_list)
    if response != None:
        resp_string = ''.join(response)
        return resp_string
    else:
        return ''