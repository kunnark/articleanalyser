import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
import statistics

def draw_normal_distribution(data, headline):
    '''
    :param data: values as list
    :param headline: headline as string
    :return: draws a plot
    '''
    plt.style.use('seaborn')

    mu = statistics.mean(data)
    std = statistics.stdev(data)
    sigma = std

    x_min = min(data)
    x_max = max(data)

    x = np.linspace(mu - 3 * sigma, mu + 3 * sigma, 1000)

    # https://stackoverflow.com/questions/38141951/why-does-scipy-norm-pdf-sometimes-give-pdf-1-how-to-correct-it
    # https://www.itl.nist.gov/div898/handbook/eda/section3/eda364.htm
    p = stats.norm.pdf(x, mu, std)

    plt.plot(x, p, 'k', linewidth=1, color='#094D78')

    # Fill area
    # Idea from https://www.science-emergence.com/Articles/How-to-plot-a-normal-distribution-with-matplotlib-in-python-/


    first_min = x_min
    first_max = mu - 2 * std
    second_min = mu - 2 * std
    second_max = mu - std
    third_min = mu - std
    third_max = mu + std
    fourth_min = mu + std
    fourth_max = mu + 2 * std
    fifth_min = mu + 2 * std
    fifth_max = x_max

    if x_min >= mu - 2 * std and x_min < mu - std:
        first_min = x_min
        first_max = x_min
        second_min = x_min
        second_max = mu - std
    elif x_min >= mu - std and x_min < mu:
        first_min = x_min
        first_max = x_min
        second_min = x_min
        second_max = x_min
        third_min = x_min
    elif x_max > mu and x_max <= mu + std:
        third_min = mu
        third_max = x_max
        fourth_min = x_max
        fourth_max = x_max
        fifth_min = x_max
        fifth_max = x_max
    elif x_max > mu + std and x_max <= mu + 2 * std:
        fourth_min = mu + std
        fourth_max = x_max
        fifth_min = x_max
        fifth_max = x_max


    pt_dict = {
        "1": [first_min, first_max],
        "2": [second_min, second_max],
        "3": [third_min, third_max],
        "4": [fourth_min, fourth_max],
        "5": [fifth_min, fifth_max]
    }

    colors_dict = {
        "1": "#0F5886",
        "2": "#1F96E1",
        "3": "#91D3FC",
        "4": "#1F96E1",
        "5": "#0F5886"
    }

    label = {
        "1": "[min; keskmine - 2 * σ]",
        "2": "[keskmine - 2 * σ; keskmine - σ]",
        "3": "[keskmine - σ; keskmine + σ]",
        "4": "[keskmine + σ; keskmine + 2 * σ]",
        "5": "[keskmine + 2 * σ; max]"
    }

    for item in pt_dict:
        pt1 = pt_dict[item][0]
        pt2 = pt_dict[item][1]

        plt.plot([pt1, pt1], [0.0, stats.norm.pdf(pt1, mu, std)], color='#094D78')
        plt.plot([pt2, pt2], [0.0, stats.norm.pdf(pt2, mu, std)], color='#094D78')

        ptx = np.linspace(pt1, pt2, 100)
        pty = stats.norm.pdf(ptx, mu, std)

        l = label[item]
        plt.fill_between(ptx, pty, color=colors_dict[item], alpha=0.5, label=None)


    # Fill the drawing
    # Style
    title = headline + ": keskmine = %.2f,  standardhälve = %.2f" % (mu, std)
    plt.xlabel("Väärtused")
    plt.ylabel("Tõenäousustihedus")
    plt.title(title, fontsize='14')

    plt.legend()
    # Style: texts

    min_text = "min=%.2f" % (x_min)
    max_text = "max=%.2f" % (x_max)
    plt.text(x_min * -1.1, int(plt.ylim()[0]) * 1.1, min_text, horizontalalignment='center', verticalalignment = 'center')
    plt.text(x_max * 1.1, int(plt.ylim()[0]) * 1.1, max_text, horizontalalignment='center', verticalalignment = 'center')

    plt.show()

