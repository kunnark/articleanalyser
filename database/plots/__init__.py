import plots.normal_distribution as plot
import pandas


# Import data:
df = pandas.read_csv("../data/values.csv")
df_lda = pandas.read_csv("../data/lda.csv")

col_headers = list(df)
col_headers_lda = list(df_lda)
# print(col_headers_lda)
cossim_list = []
tfidf_list = []
bm25_list = []
lda_list = []
for index, row in df.iterrows():
    cossim_list.append(row['cosine_similarity_value'])
    tfidf_list.append(row['tfidf_value'])
    bm25_list.append(row['bm25_value'])

for index, row in df_lda.iterrows():
    lda_list.append(row['jensen_shannon_value'])

plot.draw_normal_distribution(cossim_list, "Koosinussarnasuse normaaljaotus")
plot.draw_normal_distribution(tfidf_list, "TF-IDF normaaljaotus")
plot.draw_normal_distribution(bm25_list, "BM25 normaaljaotus")
plot.draw_normal_distribution(lda_list, "Jensen-Shannon distantsi normaaljaotus")