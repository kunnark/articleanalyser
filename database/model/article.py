class Article:
    """Article class represents news article"""
    errID = 0
    headline = ""
    lead = ""
    url = ""
    content = ""
    author = ""
    datePublished = ""
    editor = ""
    category = ""
    language = ""

    def __init__(self):
        """ Create a new article at the origin """
        self.errID = 0
        self.headline = ""
        self.lead = ""
        self.url = ""
        self.content = ""
        self.author = ""
        self.datePublished = ""
        self.editor = ""
        self.category = ""
        self.language = ""

    def get_as_dict(self):
        return {
            "errID":self.errID,
            "headline":self.headline,
            "lead":self.lead,
            "url":self.url,
            "content":self.content,
            "author":self.author,
            "editor":self.editor,
            "datePublished":self.datePublished,
            "category":self.category,
            "language":self.language
        }
    def set_article_from_dict(self, article):
        self.errID = article["errID"]
        self.headline = article["headline"]
        self.lead = article["lead"]
        self.url = article["url"]
        self.content = article["content"]
        self.author = article["author"]
        self.datePublished = article["datePublished"]
        self.editor = article["editor"]
        self.category = article["category"]
        self.language = article["language"]
