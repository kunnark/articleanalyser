from estnltk import Text
from estnltk.taggers import VabamorfAnalyzer

# https://estnltk.github.io/estnltk/1.4/tutorials/morf_tables.html
types_map = {
    "A":"omadussona_algvorre",
    "C":"omadussona_keskvorre",
    "D":"maarsona",
    "G":"genitiiv_atribuut",
    "H":"parisnimi",
    "I":"hyydsona",
    "J":"sidesona",
    "K":"kaassona",
    "N":"pohiarvsona",
    "O":"jargarvsona",
    "P":"asesona",
    "S":"nimisona",
    "U":"omadussona",
    "V":"tegusona",
    "X":"sona_verbi_juures_tahendus_puudub",
    "Y":"lyhend",
    "Z":"lausemark"
}

def get_word_type_as_name(pos_tag):
    try:
        if "|" in pos_tag:
            return types_map[list(pos_tag)[0]] # simplifying analysis by removing parallel word types
        else:
            return types_map[pos_tag]
    except TypeError:
        pass

def get_postag_of_word(word):
    try:
        # 1.4.1:
        # t = estnltk.Text(word)
        # response = (t.postags)[0]

        # 1.6.5
        # Initialize morphological analyser
        # commiting comment
        text = Text(word)
        vm_analyser = VabamorfAnalyzer()
        text.tag_layer(['sentences'])
        vm_analyser.tag(text)
        response = []
        vormide_list = text.morph_analysis['partofspeech']
        [response.append(item[0]) for item in vormide_list]
        final_response = response[0]
        return final_response
    except IndexError:
        pass
