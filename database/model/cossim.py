from collections import Counter
from math import sqrt
from math import pow

class CosSim:
    def calc_cos_sim(self) -> float:
        nominator = self.nominator()
        denominator = self.denominator_part(self.string1) * self.denominator_part(self.string2)
        return float(nominator/denominator)

    def nominator(self):
        string1_vector = self.count_word_occurences(self.string1)
        string2_vector = self.count_word_occurences(self.string2)
        n = sum(list(map(lambda word: string1_vector[word] * string2_vector[word], string1_vector)))
        return n

    def denominator_part(self, string):
        string_vector = self.count_word_occurences(string)
        d = sqrt(sum(list(map(lambda word: pow(string_vector[word], 2), string_vector))))
        return d

    def count_word_occurences(self, string) -> Counter:
        return Counter((string).split(self.delimitter))

    def __init__(self, string1, string2, delimitter):
        self.string1 = string1
        self.string2 = string2
        self.delimitter = delimitter