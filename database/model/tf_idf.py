from math import log
import helper.convert_article as convert
# Original code (C): Ahti Lohk

def docs_avg_len(init_doc_dict):
    total = 0
    for value in init_doc_dict.values():
        total += len(value.split())
    return total / len(init_doc_dict.keys())

def len_normalizer(b, doc, avg_doc):
    d = len(doc.split())
    return 1 - b + b * (d / avg_doc)

def term_freq(init_doc_dict):
    term_freq_dict = init_doc_dict.copy()
    for key, value in init_doc_dict.items():
        words_set = set(value.split())
        term_freq_dict[key] = dict()
        for word in words_set:
            word_freq = value.count(word)
            term_freq_dict[key][word] = word_freq
    return term_freq_dict

def query_doc_freq(query):
    words_list = query.split()
    words_set = set(words_list)
    query_doc_freq_dict = dict()
    for word in words_set:
        word_freq = words_list.count(word)
        query_doc_freq_dict[word] = word_freq
    return query_doc_freq_dict

def tf_transformer(term_freq):
    return log(1 + log(1 + term_freq))

def idf(term_freq_dict, query):
    query_words_set = set(query.split())
    idf_dict = dict()
    doc_freq = 0
    nr_of_doc = len(term_freq_dict.keys())
    for word in query_words_set:
        for doc, value in term_freq_dict.items():
            if word in term_freq_dict[doc]:
                doc_freq += 1
# Recommendation from supervisor, change idf_dict[word] = 0
        if doc_freq == 0:
            idf_dict[word] = 0
        else:
            idf_dict[word] = abs(log((nr_of_doc+1) / doc_freq))
    return idf_dict

def docs_scores(init_doc_dict, query_string):
    avg_doc = docs_avg_len(init_doc_dict)
    term_freq_dict = term_freq(init_doc_dict)
    query_words_freq = query_doc_freq(query_string)
    query_words_set = set(query_string.split())
    idf_dict = idf(term_freq_dict, query_string)
    docs_rank_dict = dict()
    for doc, value in term_freq_dict.items():
        total_score = 0
        for word in query_words_set:
            if word in term_freq_dict[doc]:
                doc_term_freq = term_freq_dict[doc][word]
            else:
                doc_term_freq = 0
            query_term_freq = query_words_freq[word]
            word_score = query_term_freq * tf_transformer(doc_term_freq) * idf_dict[word]
            total_score += word_score
        docs_rank_dict[doc] = round(total_score, 3)
    return docs_rank_dict


def calculate_tf_idf(comparable_string, query_string) -> float:
    comparable_string = convert.single_article_as_string(comparable_string, " ")
    query_string = convert.single_article_as_string(query_string, " ")
    init_doc_dict = {
        "comparable_string": comparable_string,
    }
    docs_scores_dict = docs_scores(init_doc_dict, query_string)
    return float(docs_scores_dict['comparable_string'])

