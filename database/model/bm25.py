from gensim.summarization.bm25 import BM25

class CalculateBM25:
    def __init__(self, queried_document, comparable_document, delimitter):
        self.queried_document = queried_document # string
        self.comparable_document = comparable_document # string
        self.delimitter = delimitter

    def __repr__(self):
        return "BM25='%s'" % (
            self.calculate_bm25_value())

    def document_as_list(self, document) -> list:
        return list(document.split(self.delimitter))

    # Calculation based on 2 documents
    def calculate_bm25_value(self) -> float:
        corpus = [self.document_as_list(self.queried_document), self.document_as_list(self.comparable_document)]
        bm25 = BM25(corpus)
        # average_idf = sum(map(lambda corpus: float(bm25.idf[corpus]), bm25.idf.keys())) / len(bm25.idf.keys())
        single_doc_bm25_score = abs(bm25.get_score(self.document_as_list(self.queried_document), 1))
        return single_doc_bm25_score
