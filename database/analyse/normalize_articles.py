import database.analyse.get_dataframe as df
from estnltk.taggers import VabamorfAnalyzer
from estnltk import Text
import database.model.wordtypes as wt
import time
import sqlalchemy
import timeout_decorator
import warnings


warnings.filterwarnings('ignore') # turn off warnings
logfile = "errors.log"

def is_included_word(included_word_types, word):
    if wt.get_word_type_as_name(wt.get_postag_of_word(word)) in included_word_types and word is not "" or word is not None:
        if "|" in word:
            # word has to be split
            word = word.split('|')[0]
            return word
        else:
            return word
    else:
        return ""


@timeout_decorator.timeout(1, use_signals=True)
def normalise_text(errored_articles, included_word_types, existing_data_as_list, *text):
    # dataframe_existing = df.get_all_articles_as_dataframe("err_news_cleared")
    # text is array of tuples as lambda input function
    try:
        responselist = []
        err_id = text[0]
        # print(existing_data_as_list)
        if err_id not in existing_data_as_list and str(err_id) not in errored_articles:
            print(err_id)
            for item in text:
                item = str(item)
                if item.isdigit() is False:
                    # print(item)
                        # 1.4.1:
                        # lemmad = (estnltk.Text(item)).tokenize_words().clean().lemmas

                        # estnltk 1.6.5:
                        # https://github.com/estnltk/estnltk/blob/version_1.6/tutorials/nlp_pipeline/B_03_segmentation_words.ipynb
                    try:
                        text = Text(item.lower())
                        vm_analyser = VabamorfAnalyzer()
                        text.tag_layer(['sentences'])
                        vm_analyser.tag(text)

                        lemmad = []
                        list_of_lemmas = text.morph_analysis['lemma']
                        [lemmad.append(item[0]) for item in list_of_lemmas]
                    except SystemError:
                        pass
                    validated_words = [is_included_word(included_word_types, word) for word in lemmad]
                    validated_words = filter(None, validated_words)  # remove empty strings from list
                    responselist.append(",".join(validated_words))
                else:
                    # add int value (err_id)
                    responselist.append(item)
            return tuple(responselist)
        else:
            # print("pass")
            pass
    except timeout_decorator.timeout_decorator.TimeoutError:
        with open(logfile, "a") as file:
            file.write(str(err_id)+"\n")
        file.close()
        print("Sain vea.")



''' Execute database insertion '''
start_time = time.time()
dataframe = df.get_all_articles_as_dataframe("err_news_related")
dataframe_existing = df.get_all_articles_as_dataframe("err_news_cleared_without_ne")
# dataframe_existing = None

existing_err_id_as_list = []
if dataframe_existing is not None:
    dataframe_existing = dataframe_existing.drop_duplicates()
    existing_err_id_as_list = dataframe_existing['err_id'].tolist()
    existing_err_id_as_list = [int(0 if i is None or 'n' in i else i) for i in existing_err_id_as_list]

# Exclude existing dataframe in db
dataframe = dataframe.dropna(subset=['content'])[(dataframe['language'] == "EST")] # puhastame NaN väärtustest

# Start inserting with dataframe
count_of_rows = dataframe['id'].count() # count dataframe

print(count_of_rows)
min = 0
max = 0
interval = 100
steps = int(round(count_of_rows / interval)) + 1

for step in range(1, steps):
    max = step * interval  # 100
    min = max - interval + 1

    if max >= count_of_rows:
        max = count_of_rows

    included_word_types = [
        "nimisona",
        "maarsona",
        "tegusona",
        "omadussona"
        # "parisnimi"
    ]

    try:
        skip_errored_articles = [line.rstrip('\n') for line in list(open(logfile, "r"))]
    except FileNotFoundError:
        open(logfile, "w+")

    try:
        dataframe_temp = dataframe[['err_id', 'headline', 'lead', 'content']].iloc[min:max+1].apply(
                                lambda row: normalise_text(
                                                        skip_errored_articles,
                                                        included_word_types,
                                                        existing_err_id_as_list,
                                                        row['err_id'],
                                                        row['headline'],
                                                        row['lead'],
                                                        row['content']
                                                        ),
                                                        raw=True, axis=1, result_type='broadcast', reduce=False
                                )
    except SystemError:
        pass
    # Write to db
    engine = sqlalchemy.create_engine('postgresql://kunnark:@localhost:5432/kunnark')
    try:
        if dataframe_temp is not None:
            dataframe_temp.to_sql(
                'err_news_cleared_without_ne',
                con=engine,
                if_exists="append",
                index_label="id",
                index=False
            )
            dataframe_temp = None
        else:
            dataframe_temp = None
    except timeout_decorator.TimeoutError or AttributeError:
        pass
    print(str(max)+" | step time:" + str(time.time() - start_time))
    if max > len(existing_err_id_as_list) and max % 1000 == 0:
        time.sleep(60)
    if max % 1000 == 0:
        print(str(max))

# engine.execute("SELECT * FROM err_news_cleared").fetchall()
print("Exec time:" +str(time.time() - start_time))
