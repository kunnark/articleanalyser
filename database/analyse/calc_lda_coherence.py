from estnltk import Text
from estnltk.taggers import VabamorfAnalyzer
import gensim
from gensim import corpora
import warnings
from gensim.models.coherencemodel import CoherenceModel
import analyse.get_dataframe as df

warnings.filterwarnings('ignore') # turn off warnings

df = df.get_all_articles_as_dataframe("err_news_is_related_cleared_with_ne")

passes = 10
alpha = 'auto'

count = 0
for index, row in df.iterrows():
    print("Step:", count)
    count += 1
    if count > 10:
        exit(1)
    article_1 = "".join(row['child_headline'] + row['child_lead'] + row['child_content'])
    article_2 = "".join(row['parent_headline'] + row['parent_lead'] + row['parent_content'])
    strings = [article_1, article_2]

    # Preprocessing
    list_of_doc_lemmas= []
    list_of_lemmas = []
    for d in strings:
        text_obj = Text(d)
        vm_analyser = VabamorfAnalyzer()
        text_obj.tag_layer(['sentences'])
        vm_analyser.tag(text_obj)
        lemmas = []
        e_lemmalist = text_obj.morph_analysis['lemma']
        [lemmas.append(item[0]) for item in e_lemmalist]
        [list_of_lemmas.append(item[0]) for item in e_lemmalist]
        list_of_doc_lemmas.append(lemmas)

    # Model building
    common_dictionary = corpora.Dictionary()
    common_dictionary.add_documents(list_of_doc_lemmas)
    corpus = [common_dictionary.doc2bow(document) for document in list_of_doc_lemmas]

    # Train the model
    Lda = gensim.models.ldamodel.LdaModel

    coherence_val = dict()
    coherence_data = list()

    for i in range(1, 101):
        lda_model = Lda(corpus=corpus, num_topics=i, id2word=common_dictionary, alpha=alpha)
        cm = CoherenceModel(model=lda_model, corpus=corpus, coherence='u_mass')
        coherence = cm.get_coherence()
        coherence_val["doc_id"] = count
        coherence_val["topics"] = i
        coherence_val["coherence"] = coherence
        coherence_data.append(coherence_val)
        coherence_val = {}
        f = open("coherence.txt", "a+")

    for item in coherence_data:
        f.write(str(item["doc_id"]) + "\t" + str(item["topics"]) + "\t" + str(item["coherence"]) + "\n")
    f.close()

