import warnings
import logging
import model.jensen_shannon_lda as lda
import analyse.get_dataframe as df
import time
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, Sequence, MetaData, Table, Float
from sqlalchemy.ext.declarative import declarative_base

# Use Python 3.6 environment
start_time = time.time()
logging.basicConfig(filename='lda_model.log', format='%(asctime)s : %(levelname)s : %(message)s', level=logging.ERROR)
warnings.filterwarnings('ignore') # turn off warnings

# Input for calculation
number_of_topics = 2
passes = 50
alpha = 'auto'
response = None

Base = declarative_base()
class AnalysisResults(Base):
    __tablename__ = 'err_news_analysis_results_with_ne_lda'
    id = Column(Integer, Sequence('err_news_analysis_results_with_ne_lda_id_seq'), primary_key=True)
    child_err_id = Column(String)
    parent_err_id = Column(String)
    jensen_shannon_value = Column(Float)

    def __repr__(self):
        return "<Article(id='%s', parent_err_id='%s', child_err_id='%s')>" % (
                                self.id, self.parent_err_id, self.child_err_id)

def write_to_database(number_of_topics, passes, alpha) -> bool:
    engine = db.create_engine('postgresql://127.0.0.1:5432/kunnark')
    connection = engine.connect()
    Session = sessionmaker()
    Session.configure(bind=engine)
    session = Session()
    metadata = MetaData()

    calculated_article_pairs = calc_related_article_pairs(number_of_topics, passes, alpha)
    table = Table('err_news_analysis_results_with_ne_lda', metadata, autoload=True, autoload_with=engine)

    for article in calculated_article_pairs:
        article_writeable = AnalysisResults(
            child_err_id=article[-1]['child_err_id'],
            parent_err_id=article[-1]['parent_err_id'],
            jensen_shannon_value=article[-1]['jensen_shannon_value']
        )
        session.add(article_writeable)
        session.commit()
    connection.close()
    return True

def calc_related_article_pairs(number_of_topics, passes, alpha) -> dict:
    is_related_and_cleared_df = df.get_all_articles_as_dataframe("err_news_is_related_cleared_with_ne")

    result = {
        "parent_err_id" :"",
        "child_err_id" :"",
        "jensen_shannon_value" :"",
    }
    response = []
    # Calculate LDA model over document pairs:
    for index, row in is_related_and_cleared_df.iterrows():
        child_article = "".join(row['child_headline'] + row['child_lead'] + row['child_content'])
        parent_article = "".join(row['parent_headline'] + row['parent_lead'] + row['parent_content'])

        jensen_shannon = lda.jensen_shannon_value(parent_article.split(","), child_article.split(","), number_of_topics, passes, alpha)

        result['parent_err_id'] = row['parent_err_id']
        result['child_err_id'] = row['child_err_id']
        result['jensen_shannon_value'] = jensen_shannon
        response.append(result)
        result = {}

        if index % 100 == 0 and index > 0:
            print("Index="+str(index)+ " Step time: "+str(time.time() - start_time))
        yield response

write_to_database(number_of_topics, passes, 'auto')


