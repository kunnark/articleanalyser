import psycopg2
from database.config.config import config
import pandas.io.sql as sqlio

# Example: https://gist.github.com/jakebrinkmann/de7fd185efe9a1f459946cf72def057e
def get_all_articles_as_dataframe(table_name):
    params = config()
    conn = psycopg2.connect(** params)
    if conn is not None:
        sql = "SELECT * FROM "+table_name+";"
        dataframe = sqlio.read_sql_query(sql, conn)
        if dataframe.empty is False:
            conn.close()
            return dataframe
    else:
        conn.close()
        return None

