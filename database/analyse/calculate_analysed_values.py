import analyse.get_dataframe as df
import warnings
import time
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, Sequence, MetaData, Table, Float, Boolean
from sqlalchemy.ext.declarative import declarative_base
from model.bm25 import CalculateBM25
import model.tf_idf as CalculateTDIDF
from model.cossim import CosSim

start_time = time.time()
warnings.filterwarnings('ignore') # turn off warnings

def calc_related_article_pairs() -> dict:
    is_related_and_cleared_df = df.get_all_articles_as_dataframe("err_news_is_related_cleared_without_ne")
    # is_not_related_and_cleared_df
    result = {
        "parent_err_id":"",
        "child_err_id":"",
        "cossim_value":"",
        "bm25_value":"",
        "tf_idf_value":""
    }
    response = []
    for index, row in is_related_and_cleared_df.iterrows():
        child_article = "".join(row['child_headline'] + row['child_lead'] + row['child_content'])
        parent_article = "".join(row['parent_headline'] + row['parent_lead'] + row['parent_content'])

        cossim = CosSim(child_article, parent_article, ",")
        cossim_value = cossim.calc_cos_sim()
        bm25_value = CalculateBM25(parent_article, child_article, ",").calculate_bm25_value()
        tf_idf_value = CalculateTDIDF.calculate_tf_idf(parent_article, child_article)

        result['parent_err_id'] = row['parent_err_id']
        result['child_err_id'] = row['child_err_id']
        result['cossim_value'] = cossim_value
        result['bm25_value'] = bm25_value
        result['tf_idf_value'] = tf_idf_value
        response.append(result)
        result = {}
    return response


# @timeout_decorator.timeout(60, use_signals=True)
def write_to_database() -> bool:

        engine = db.create_engine('postgresql://127.0.0.1:5432/kunnark')
        connection = engine.connect()
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        metadata = MetaData()
        calculated_article_pairs = calc_related_article_pairs()
        table = Table('err_news_analysis_results_without_ne', metadata, autoload=True, autoload_with=engine)

        for article in calculated_article_pairs:
            article_writeable = AnalysisResults(
                child_err_id = article['child_err_id'],
                parent_err_id= article['parent_err_id'],
                cosine_similarity_value = article['cossim_value'],
                bm25_value = article['bm25_value'],
                lda_value = None,
                is_related_articles = True,
                tfidf_value = article['tf_idf_value']
            )
            session.add(article_writeable)
            session.commit()
        connection.close()
        return True

Base = declarative_base()
class AnalysisResults(Base):
    __tablename__ = 'err_news_analysis_results_without_ne'
    id = Column(Integer, Sequence('err_news_analysis_results_id_seq'), primary_key=True)
    child_err_id = Column(String)
    parent_err_id = Column(String)
    cosine_similarity_value = Column(Float)
    bm25_value = Column(Float)
    lda_value = Column(Float)
    is_related_articles = Column(Boolean)
    tfidf_value = Column(Float)

    def __repr__(self):
        return "<Article(id='%s', parent_err_id='%s', child_err_id='%s')>" % (
                                self.id, self.parent_err_id, self.child_err_id)


write_to_database()
print("Exec time:" +str(time.time() - start_time))