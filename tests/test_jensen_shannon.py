import logging
import numpy
from gensim.corpora import Dictionary
from gensim.models import ldamodel
from gensim.matutils import jensen_shannon

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

'''
    Sample code: https://radimrehurek.com/gensim/auto_examples/tutorials/run_distance_metrics.html
'''

def parse_topic_string(topic):
    # takes the string returned by model.show_topics()
    # split on strings to get topics and the probabilities
    topic = topic.split('+')
    # list to store topic bows
    topic_bow = []
    for word in topic:
        # split probability and word
        prob, word = word.split('*')
        # get rid of spaces and quote marks
        word = word.replace(" ", "").replace('"', '')
        # convert to word_type
        word = model.id2word.doc2bow([word])[0][0]
        topic_bow.append((word, float(prob)))
    return topic_bow

texts = [
    ['finance', 'money', 'sell'],
    ['river', 'water', 'shore', 'bank', 'house', 'horse', 'swing']
]

dictionary = Dictionary(texts)
corpus = [dictionary.doc2bow(text) for text in texts]

numpy.random.seed(1)  # setting random seed to get the same results each time.
model = ldamodel.LdaModel(corpus, id2word=dictionary, num_topics=2, minimum_probability=1e-8)

doc_water = ['finance', 'money', 'sell'] # ['river', 'water', 'shore', 'bank']
doc_finance = ['river', 'water', 'shore', 'bank', 'house', 'horse', 'swing']

bow_water = model.id2word.doc2bow(doc_water)
bow_finance = model.id2word.doc2bow(doc_finance)

# we can now get the LDA topic distributions for these
lda_bow_water = model[bow_water]
lda_bow_finance = model[bow_finance]
print("lda_bow_water", lda_bow_water)
print("Jensen-Shannon:", jensen_shannon(lda_bow_water, lda_bow_finance))


# ver 2:
# return ALL the words in the dictionary for the topic-word distribution.
topic_water, topic_finance = model.show_topics(num_words=len(model.id2word))

print(topic_water)
print(topic_finance)
# do our bag of words transformation again
finance_distribution = parse_topic_string(topic_finance[1])
water_distribution = parse_topic_string(topic_water[1])

# and voila!
print(jensen_shannon(water_distribution, finance_distribution))
''