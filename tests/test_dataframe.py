import database.analyse.get_dataframe as df
import warnings

warnings.filterwarnings('ignore') # turn off warnings

def remove_existing(df_comparable, *rows):
    comparable_list = df_comparable['err_id'].tolist()
    err_id = rows[1]
    if err_id not in comparable_list:
        # print(rows)
        return rows
    else:
        casted = list(rows)
        casted[1] = -1
        # print(casted)
        return tuple(casted)


def get_remained_dataframe():
    df1 = df.get_all_articles_as_dataframe("err_news_orig")
    df2 = df.get_all_articles_as_dataframe("err_news_cleared")
    # print(type(df2))
    # print(df2.size)
    if df2 is not None:
        # df2['err_id'] = df2['err_id'].fillna(-1, inplace=True)
        # Probleem err_id tunneb NaN ja ei oska astype meetod midagi teha ja viskab vea: ValueError: invalid literal for int() with base 10: 'nan'
        print(df2.head(n=5))
        print((df2['err_id'].isna()).count())
        df2['err_id'] = df2['err_id'].astype(int)
        df1 = df1.dropna(subset=['content'])[(df1['language'] == "EST")]
        df1['err_id'] = df1['err_id'].astype(int)
            # df3 = pd.concat([df1, df2], join='outer')
            # df4 = df3.drop_duplicates(subset="err_id", keep='first')
        df5 = df1[['id', 'err_id', 'headline', 'lead', 'content']]\
                .apply(lambda row: remove_existing(df2, row['id'], row['err_id'], row['headline'], row['lead'],
                                                   row['content']
                                                   ), raw=True, broadcast=True, axis=1, reduce=False)
        df6 = df5.where(df5['err_id'] != -1)
            # df6[['headline', 'lead', 'content']].astype(str)
        return df6
    else:
        df1 = df1.dropna(subset=['content'])[(df1['language'] == "EST")]
        return df1
    # print("df1 " + str(df1['err_id'].count()))
    # print("df3 " + str(df3['err_id'].count()))
    # print("df4 " + str(df4['err_id'].count()))
    # print("df5 " + str(df5['err_id'].count()))
    # print("df6 " + str(df6['err_id'].count()))