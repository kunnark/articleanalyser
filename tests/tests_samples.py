import estnltk
lause = "Antslas on II pensionisamba vabastamise aastapäeva pidu. Kuid raha on otsas."
text = estnltk.Text(lause)
tokenized = text.tokenize_words().lemmas
print(tokenized)

lause = "Suure-Jaanis on II pensionisamba vabastamise aastapäeva pidu. Raha peo pidamiseks sai otsa."
text = estnltk.Text(lause)
tokenized = text.tokenize_words().words
print(tokenized)

words = ['Andrus', 'üheksa', 'koer', 'ümmargune', 'jookseb']
result = [estnltk.Text(word).postags[0] for word in words]
print(result)