from estnltk import Text
from estnltk.taggers import VabamorfAnalyzer
import gensim
from gensim import corpora
import warnings
import logging
from gensim.test.utils import datapath
import numpy as np
import scipy.stats
from gensim.models.coherencemodel import CoherenceModel

def jensen_shannon_distance(p, q):
    """
    method to compute the Jenson-Shannon Distance
    between two probability distributions
    https://medium.com/@sourcedexter/how-to-find-the-similarity-between-two-probability-distributions-using-python-a7546e90a08d
    """
    # convert the vectors into numpy arrays in case that they aren't
    p = np.array(p)
    q = np.array(q)
    # calculate m
    m = (p + q) / 2
    # compute Jensen Shannon Divergence
    divergence = (scipy.stats.entropy(p, m) + scipy.stats.entropy(q, m)) / 2
    # compute the Jensen Shannon Distance
    distance = np.sqrt(divergence)
    return distance

logging.basicConfig(filename='lda_model.log', format='%(asctime)s : %(levelname)s : %(message)s', level=logging.ERROR)
warnings.filterwarnings('ignore') # turn off warnings


article_1 = "Tartu Ülikoolis tehtud analüüs näitas, et vanus 80 ja enam aastat, naissugu, läbipõetud depressiivsus ja vähene füüsiline aktiivsus on Eesti kesk- ja vanemaealiste depressiivsuse tekkes olulised tegurid. Uuringu tulemustest nähtub, et kuna depressiivsete riskirühma kuulub üha enam eakaid, peaks mõtlema perearsti visiidi aja pikendamisele, et ennetada vaimse tervise halvenemist."
article_2 = "Kõigil meil on mõnikord pingelisemaid aegu. Kui need läbi saavad, siis võib võtta veel mõnda aega, enne kui suudame elust jälle täiel rinnal rõõmu tundma hakata. Või kuidas kunagi, või kuidas kellelgi. Asi sõltub, nagu tihtilugu ikka, asjaoludest." # "Kass Artur  Mari käisid marjul Nad jooksid üle aasa ning võtsid mustikad kaasa jah"
number_of_topics = 25
passes = 50
alpha = 0.5

strings = [article_1, article_2]

# Preprocessing
list_of_doc_lemmas= []
list_of_lemmas = []
for d in strings:
    text_obj = Text(d)
    vm_analyser = VabamorfAnalyzer()
    text_obj.tag_layer(['sentences'])
    vm_analyser.tag(text_obj)
    lemmas = []
    e_lemmalist = text_obj.morph_analysis['lemma']
    [lemmas.append(item[0]) for item in e_lemmalist]
    [list_of_lemmas.append(item[0]) for item in e_lemmalist]
    list_of_doc_lemmas.append(lemmas)


# Model building
common_dictionary = corpora.Dictionary()
common_dictionary.add_documents(list_of_doc_lemmas)
corpus = [common_dictionary.doc2bow(document) for document in list_of_doc_lemmas]

# Train the model
Lda = gensim.models.ldamodel.LdaModel

coherence_val = dict()
coherence_data = list()
for i in range(1, 100):
    lda_model = Lda(corpus=corpus, num_topics=i, id2word=common_dictionary, passes=passes, alpha=alpha)
    cm = CoherenceModel(model=lda_model, corpus=corpus, coherence='u_mass')
    coherence = cm.get_coherence()
    coherence_val["topics"] = i
    coherence_val["coherence"] = coherence
    coherence_data.append(coherence_val)
    coherence_val = {}

print(coherence_data)

lda_model = Lda(corpus=corpus, num_topics=number_of_topics, id2word=common_dictionary, passes=passes, alpha=alpha)

# Save the model
temp_file = datapath("model.lda")
lda_model.save(temp_file)

# Compare the articles:
text1 = list_of_doc_lemmas[0] # Pärime article_1 kohta
bow1 = common_dictionary.doc2bow(text1)

text2 = list_of_doc_lemmas[1] # Pärime article_1 kohta
bow2 = common_dictionary.doc2bow(text2)

# Artikkel 1 topicud:
print("Concrete document #1 topics: " + str(lda_model.get_document_topics(bow1))) # Välja tuleb list teema id ja kogu topicute seosega.

article_1_probability_distribution = lda_model.get_document_topics(bow1).pop()
# Artikkel 2 topicud:
print("Concrete document #2 topics: " + str(lda_model.get_document_topics(bow2))) # Välja tuleb list teema id ja kogu topicute seosega.
article_2_probability_distribution = lda_model.get_document_topics(bow2).pop()


jensen_shannon = jensen_shannon_distance(article_1_probability_distribution, article_2_probability_distribution)
print("Trained model topics:" + str(lda_model.show_topics(num_topics = number_of_topics, num_words=5)))

# Tagastame Jensen-Shannoni väärtuse: [0, 1.0] - mida väiksem on distants, seda lähedasemad sisult artiklid on.
value = np.float64(jensen_shannon)
print(value)
value = float(np.atleast_1d(value).astype(list))
print(value)