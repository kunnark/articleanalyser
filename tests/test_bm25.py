from gensim.summarization.bm25 import BM25
# Initialize corpus
document1 = "Kui Arno isaga kooli jõudis, olid tunnid juba alanud."
document2 = "Arno ei tahtnud isaga kooli minna."
corpus = [list(document1.split(" ")), list(document2.split(" "))]
bm25 = BM25(corpus)
# https://stackoverflow.com/questions/40966014/how-to-use-gensim-bm25-ranking-in-python
average_idf = sum(map(lambda corpus: float(bm25.idf[corpus]), bm25.idf.keys())) / len(bm25.idf.keys())
single_doc_bm25_score = bm25.get_scores(list(document1.split(" ")))
print("BM25 Single Document Score: "+str(single_doc_bm25_score))


