from estnltk.taggers import VabamorfAnalyzer
from estnltk import Text

# https://github.com/estnltk/estnltk/blob/version_1.6/tutorials/nlp_pipeline/B_03_segmentation_words.ipynb

text = Text("Kadri")
vm_analyser = VabamorfAnalyzer()
text.tag_layer(['sentences'])
vm_analyser.tag(text)

lemmas = []
list_of_lemmas = text.morph_analysis['lemma']
[lemmas.append(item[0]) for item in list_of_lemmas]

print(lemmas)


# Sõnavorm:
vormid = []
vormide_list = text.morph_analysis['partofspeech']
[vormid.append(item[0]) for item in vormide_list]
print(vormid)